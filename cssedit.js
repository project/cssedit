
/* -----------------------------------------------------------------

  CSSEditManager   

------------------------------------------------------------------ */

var CSSEditManager = CSSEditManager || {};

/**
 * Initialize.
 */
CSSEditManager.init = function() {
  // @todo move window so that it is within view on resize; use easing function to move it
  // @todo remove stylesheets and use byType?
  this.stylesheets = this.getStylesheets('themes');
	this.stylesheetsByType = {};
	this.stylesheetsByType['themes'] = this.stylesheets;
	this.stylesheetsByType['modules'] = this.getStylesheets('modules');
  this.display();
};

/**
 * Display GUI.
 */
CSSEditManager.display = function() { 
  var self = this;
  this.links = {};
  this.switcherCurrent = 'themes';  
  this.links['themes'] = this.getStylesheetLinks('themes');
  this.links['modules'] = this.getStylesheetLinks('modules');
  this.panel = $('<div id="cssedit-panel"></div>');
                
  // Add switcher
  this.switcher = $('<div id="cssedit-switcher">' + this.switcherCurrent + '</div>');
  $(this.panel).append(this.switcher);  
                     
  // Add nav item markup
  $(this.panel).append(this.links[this.switcherCurrent].join(' '));                               
  $('body').append(this.panel);
  
  this.bindStylesheetLinks();
  
  // Bind switcher events
  $(this.switcher).click(function(){
    self.cycleSwitcher();
  });
}

/**
 * Bind stylesheet link events.
 */
CSSEditManager.bindStylesheetLinks = function() {
  var self = this;
  
  $('.stylesheet-link').click(function(){
    var link = this; 
    
    // Remove old editors
    // @todo delete CSSEditor objects as well
    // @todo clicking the same editor window should close it so none are showing
    $('.cssedit-window').remove();
  
    // Create new editor
    var editor = new CSSEditor(self.getStylesheet(self.switcherCurrent, $(this).attr('id')));
     
    // Initially active
    $(this).addClass('active');
    
    // Bind window close event
    $(editor.window).bind('windowClose', function(){
      $(link).removeClass('active');  
    });
    
    return false;
  });
}

/**
 * Cycle through stylesheets shown on the panel.
 */
CSSEditManager.cycleSwitcher = function() {
  var self = this;
  
  // Cycle
  switch($(this.switcher).html()){
    case 'themes':
      self.switcherCurrent = 'modules';
      break;
      
    case 'modules':
      self.switcherCurrent = 'themes';
      break;
  }
  
  // New switcher label
  $(this.switcher).html(self.switcherCurrent);  
  
  // Clear old items
  $('.stylesheet-link', this.panel).remove();
   
  $(this.panel).append(this.links[self.switcherCurrent].join(' '));
            
  this.bindStylesheetLinks();
}

/**
 * Get stylesheets on the current page.
 *
 * @param string type
 *   Valid options are:
 *    - themes
 *    - modules
 *    - system
 *
 * @return array
 *   Link nodes.
 */
CSSEditManager.getStylesheets = function(type) {
  var self = this;
  var k = [];
  var s = $('link[rel=stylesheet]');
  var r = new RegExp('^(.*?)/' + type + '/(.*?)$', 'i');
                        
  $(s).each(function(i, ss){
    var h = $(ss).attr('href');  
    if (r.test(h)){
      ss.name = self.getStylesheetName(h); 
      k.push(ss);  
    }
  });
       
  return k;
};

/**
 * Get stylesheet node by index.
 *
 * @param string type
 * 
 * @param int i
 *
 * @return object
 */
CSSEditManager.getStylesheet = function(type, i) {
  return this.stylesheetsByType[type][i];
}

/**
 * Get name of file from href.
 *
 * @return string
 */
CSSEditManager.getStylesheetName = function(href) {
  var m = /^(?:.*?)\/([^\/]*?)\.css(?:.*)$/.exec(href); 
  return m[1];
};

/**
 * Get style sheet link markup.
 * 
 * @param string type
 *
 * @return array
 */
CSSEditManager.getStylesheetLinks = function(type) {
  var o = [];
	
  $(this.stylesheetsByType[type]).each(function(i, ss){ 
    o[i] = '<a href="#" id="' + i + '" class="stylesheet-link">' + ss.name + '</a>';
  }); 
  
  return o;
}; 

/* -----------------------------------------------------------------

  CSSEditor 

------------------------------------------------------------------ */

/**
 * Constructor.
 *
 * @param object stylesheet
 *   jQuery stylesheet node.
 */
var CSSEditor = function(stylesheet) {
  var self = this;
  this.stylesheet = stylesheet;
  this.window = new this.window(this);
};

/**
 * Constructor.
 */
CSSEditor.prototype.window = function(editor) {
  var self = this;
  this.editor = editor; 
  this.display().animateTo(window.innerHeight / 2);
  this.textarea = new editor.textarea(this);
};  

/**
 * Display window.
 *
 * @return object
 *   self. 
 */
CSSEditor.prototype.window.prototype.display = function() {
  var self = this;
  
  // Append markup  
  this.node = $('<div class="cssedit-window"><div class="content"></div></div>').get(0); 
  $('body').append(this.node);
                    
  // Add additional markup
  this.contentNode = $('.content', this.node);
  this.titlebarNode = $('<div class="cssedit-titlebar"></div>');
  this.titleNode = $('<span class="title">' + this.editor.stylesheet.name + '<em>.css</em></span>');
  $(this.titlebarNode).append(this.titleNode);
  $(this.contentNode, this.node).prepend(this.titlebarNode);
  
  // Bind events
  $(this.titlebarNode).mousedown(function(e){ self.startDrag(e) });
  $(document).mousemove(function(e){ self.drag(e) });
  $(document).mouseup(function(e){ self.endDrag(e) });
  
  return this;
}; 

/**
 * Animate to position.
 *
 * @return object
 *   self.
 */
CSSEditor.prototype.window.prototype.animateTo = function(y) { 
	var self = this;
	
	// @todo add easing function
  $(this.node).animate({
	  height: window.innerHeight - y
  }, 1000, 'easeOutElastic', function(){
		$('#cssedit-spacer').remove();
		$('body').append('<div id="cssedit-spacer" style="height: ' + (window.innerHeight - y) + 'px;"></div>');
	  $(self).trigger('animateToComplete', [y, (window.innerHeight - y)]);
  });

  return this;
};

/**
 * Initialize dragging.
 */
CSSEditor.prototype.window.prototype.startDrag = function(e) { 
	$('body').append('<div id="cssedit-outline"></div>');
  this.dragging = true;
}

/**
 * Continue dragging.
 */
CSSEditor.prototype.window.prototype.drag = function(e) {  
  if (this.dragging){  
    $('#cssedit-outline').css('top', e.clientY + 'px');
  }
}

/**
 * End dragging.
 */
CSSEditor.prototype.window.prototype.endDrag = function(e) { 
	if (this.dragging){
  	$('#cssedit-outline').remove(); 
  	this.animateTo(e.clientY);
    this.dragging = false;
  }
}

/**
 * Close the window.
 *
 * @return object
 *   self. 
 */
CSSEditor.prototype.window.prototype.close = function() {  
  $(this.node).remove();
  // Trigger close event
  $(this).trigger('windowClose');
                           
  return this;
}

/**
 * Close and destroy the editor.
 *
 * @return object
 *   self. 
 */
CSSEditor.prototype.window.prototype.destroy = function() {  
  this.close();
  delete this; 
   
  return this;
}

/* -----------------------------------------------------------------

  CSSEditor.textarea   

------------------------------------------------------------------ */

/**
 * Constructor.
 */
CSSEditor.prototype.textarea = function(window) {
  var self = this;
  this.window = window;

  // Create textarea
  $(this.window.contentNode, window.node).append('<textarea class="css"></textarea>'); 
  this.node = $('.css', window.node).get(0);
  this.node.spellcheck = false;

  // Create right panel
  this.rightNode = $('<div class="right"></div>');

  // Create save button
  this.saveNode = $('<input type="button" value="' + Drupal.t('Save') + '" class="cssedit-save" />"');
  $(this.node).after(this.rightNode);
  $(this.rightNode).append(this.saveNode);

  // Create code explorer
  this.explorer = new window.editor.explorer(this); 

  // Load css and bind key events
  this.load().bindKeyEvents();  

  // Bind saving of css
  $(this.saveNode).click(function(){ self.save() });

  // Resize when window is finished resizing
  $(this.window).bind('animateToComplete', function(e, y, height){
	  $(self.node).animate({
		  height: height - 100
	  }, 450);
  });
};

/**
 * Bind handlers specific key events.
 *
 * @return object
 *   self. 
 */
CSSEditor.prototype.textarea.prototype.bindKeyEvents = function(e) {
  var self = this;
  
  $(document).keydown(function(e){
    // Save
    if (e.keyCode == 83 && e.ctrlKey){
      self.save();
      // @todo prevent dialog  
    }
  })
  
  return this;
}

/**
 * Load stylesheet contents.
 *
 * @return object
 *   self. 
 */
CSSEditor.prototype.textarea.prototype.load = function() {
  var self = this;  
          
  $.ajax({
    url: $(self.window.editor.stylesheet).attr('href'),
    success: function(contents){    
      // Display new css
      $(self.node).val(contents);
      
      // Trigger load event
      $(self).trigger('loadStylesheet', [contents]);
                          
      return contents;
    },
    error: function(){
      // @todo new status reporting
    }
  });
      
  return this;
}

/**
 * Render stylesheet changes.
 *
 * @return object
 *   self. 
 */
CSSEditor.prototype.textarea.prototype.render = function() {   
  var s = this.window.editor.stylesheet;
  // @todo fix issue of textarea scrolling up
  $(s).attr('href', $(s).attr('href') + '?new');
  
  return this;  
}

/**
 * Save stylesheet.
 *
 * @return object
 *   self. 
 */
CSSEditor.prototype.textarea.prototype.save = function() {
  var self = this;
  var data = { filename: $(this.window.editor.stylesheet).attr('href'), css: $(this.node).val() };
        
  $(self.saveNode).attr('disabled', 'disabled');
  $.post(Drupal.settings.basePath + 'js/cssedit/save_file', data, function(response){
    // Display messages
    if (response.message){
      // @todo new set status
    }      
    // Enable button and render new style
    if (response.status == 1){
      $(self.saveNode).removeAttr('disabled');
      self.render();
    }
    // Trigger save event
    $(self).trigger('saveStylesheet', [response]);
  }, 'json'); 
  
  return this;
}

/* -----------------------------------------------------------------

  CSSEditor.explorer   

------------------------------------------------------------------ */

/**
 * Constructor.
 */
CSSEditor.prototype.explorer = function(textarea) {
  var self = this;
  this.textarea = textarea; 
          
  // Add explorer markup
  this.node = $('<div class="cssedit-explorer">'
    + '<span class="title">' + Drupal.t('Explorer') + '</span>'
    + '</div>').get(0);
  $(this.textarea.rightNode).prepend(this.node);
  
  // Parse comments
  $(this.textarea).bind('loadStylesheet', function(){
    var blocks = self.getBlocks(); 
    self.displayNavigation(blocks);
  });

  // Resize when window is finished resizing
  $(this.textarea.window).bind('animateToComplete', function(e, y, height){
	  $(self.node).animate({
		  height: height - 120
	  }, 450);
  });
};

/**
 * Get comment blocks.
 *
 * @return array
 */
CSSEditor.prototype.explorer.prototype.getBlocks = function() { 
  var b = [];
   
  // @todo finish
  var blocks = this.textarea.node.value.match(/^\s+\*\s+(.*?)$/gmi);
	if (blocks){
	  $.each(blocks, function(i, block){
	    var title = block.replace(/\s+\* /, '');
	    b.push(title);
	  });
	}
  
  return b;
}

/**
 * Display stylesheet navigation.
 *
 * @param array blocks
 */
CSSEditor.prototype.explorer.prototype.displayNavigation = function(blocks) {
  var self = this;
  
  blocks.reverse();
  $.each(blocks, function(i, title){
    var b = $('<a href="#" class="cssedit-navitem">' + title + '</a>');
    $('.title', self.node).after(b);
    $(b).click(function(){          
      var offset = self.getBlockTitleOffset(title);
      // @todo textarea scrollTo method
    });
  });
}

/**
 * Get offset of block title.
 *
 * @param int
 */
CSSEditor.prototype.explorer.prototype.getBlockTitleOffset = function(title) {
  // @todo
}

/* -----------------------------------------------------------------

  Easing Functions

------------------------------------------------------------------ */

jQuery.extend( jQuery.easing,
{
	def: 'easeOutElastic',
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	}
});

if (Drupal.jsEnabled) {
  $(function(){
    CSSEditManager.init();
  });
}




